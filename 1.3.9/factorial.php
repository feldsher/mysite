<?php
/**
 * Created by PhpStorm.
 * User: feldsher
 * Date: 07.11.17
 * Time: 19:10
 */
function calculateFactorial($number)
{
  if ($number < 0) {
    die('Number cannot be less than zero');
  } elseif ($number == 0) {
    return 1;
  }

  return $number * calculateFactorial($number - 1);
}

echo calculateFactorial(10);