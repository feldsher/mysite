<?php
/**
 * Created by PhpStorm.
 * User: feldsher
 * Date: 14.11.17
 * Time: 19:45
 */
class Computer
{
  var $cpu;
  var $ram;
  var $video;
  var $memory;
  var $isWorking = false;
  var  $computerName = 'Computer';
  function start()
  {
    $this->isWorking = true;
    echo 'Компьютер включен. <br>';
  }

  function shutDown()
  {
    $this->isWorking = false;
    echo 'Компьютер выключен. <br>' ;
  }

  function restart()
  {
    if ($this->isWorking) {
      $this->shutDown();

      for ($timer = 5; $timer > 0; $timer--) {
        echo '.';
        sleep(1);
      }
      echo "<br>";

      $this->start();
    } else
    {
      echo 'Компьютер выключен. Нужно включить<br>';
    }
  }
  function  printParameters()
  {
    $result = $this->computerName . ' must be turned on for print parameters' . PHP_EOL;
    if ($this->isWorking) {
      $result = sprintf(
        "<<<<<<<<<<\nName: %s\nCPU: %s\nRAM: %s\nVideo card: %s\nMemory: %s\n>>>>>>>>>>\n",
        $this->computerName,
        $this->cpu,
        $this->ram,
        $this->video,
        $this->memory
      );
    }
    echo $result;
  }
  function identifyUser()
  {
    echo $this->computerName . ': Identify by login and password' . PHP_EOL;
  }
}
/*
$computer = new Computer();
if ($computer instanceof Computer) {
  $computer->start();
  $computer->restart();
  $computer->shutDown();
  $computer->restart();
}*/