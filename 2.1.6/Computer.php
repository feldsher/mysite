<?php

/**
 * Class Computer
 */
class Computer
{
  /**
   * @var string
   */
  private $cpu;

  /**
   * @param string $cpu
   * @return Computer
   */
  protected function setCpu($cpu)
  {
    $this->cpu = $cpu;
    return $this;
  }

  /**
   * @return string
   */
  public function getCpu()
  {
    return $this->cpu;
  }

  /**
   * @var string
   */
  private $ram;

  /**
   * @param string $ram
   * @return Computer
   */
  protected function setRam($ram)
  {
    $this->ram = $ram;
    return $this;
  }

  /**
   * @return string
   */
  public function getRam()
  {
    return $this->ram;
  }

  /**
   * @var string
   */
  private $video;

  /**
   * @param string $video
   * @return Computer
   */
  protected function setVideo($video)
  {
    $this->video = $video;
    return $this;
  }

  /**
   * @return string
   */
  public function getVideo()
  {
    return $this->video;
  }

  /**
   * @var string
   */
  private $memory;

  /**
   * @param string $memory
   * @return Computer
   */
  protected function setMemory($memory)
  {
    $this->memory = $memory;
    return $this;
  }

  /**
   * @return string
   */
  public function getMemory()
  {
    return $this->memory;
  }

  /**
   * @var string
   */
  private $computerName = 'Computer';

  /**
   * @param string $computerName
   * @return Computer
   */
  protected function setComputerName($computerName)
  {
    $this->computerName = $computerName;
    return $this;
  }

  /**
   * @return string
   */
  public function getComputerName()
  {
    return $this->computerName;
  }

  /**
   * @var bool
   */
  private $isWorking = false;

  public function start()
  {
    $this->isWorking = true;
    echo $this->getComputerName() . ' is working' . "<hr>";
  }

  public function shutDown()
  {
    $this->isWorking = false;
    echo $this->getComputerName() . ' is off' . "<hr>";
  }

  public function restart()
  {
    if ($this->isWorking) {
      $this->shutDown();

      for ($timer = 5; $timer > 0; $timer--) {
        echo '.';
        sleep(1);
      }
      echo PHP_EOL;

      $this->start();
    } else {
      echo $this->getComputerName() . ' must be turned on for restart' . "<hr>";
    }
  }

  public function printParameters()
  {
    $result = $this->computerName . ' must be turned on for print parameters' . "<hr>";
    if ($this->isWorking) {
      $result = sprintf(
        "<<<<<<<<<<\nName: %s\nCPU: %s\nRAM: %s\nVideo card: %s\nMemory: %s\n>>>>>>>>>>\n",
        $this->getComputerName(),
        $this->getCpu(),
        $this->getRam(),
        $this->getVideo(),
        $this->getMemory()
      );
    }

    echo $result;
  }

  public function identifyUser()
  {
    echo $this->getComputerName() . ': Identify by login and password' . "<hr>";
  }
}