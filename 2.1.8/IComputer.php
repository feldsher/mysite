<?php

/**
 * Interface IComputer
 */
interface IComputer
{
    function start();

    function shutDown();

    function restart();

    function printParameters();

    function identifyUser();
}
