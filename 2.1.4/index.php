<?php
/**
 * Created by PhpStorm.
 * User: feldsher
 * Date: 14.11.17
 * Time: 19:45
 */
require_once 'Computer.php';
require_once 'Asus.php';
require_once 'Lenovo.php';
require_once 'MacBook.php';

$macBook = new MacBook();
$macBook->start();
$macBook->printParameters();
$macBook->identifyUser();
echo PHP_EOL;

$asus = new Asus();
$asus->start();
$asus->printParameters();
$asus->identifyUser();
echo PHP_EOL;

$lenovo = new Lenovo();
$lenovo->start();
$lenovo->printParameters();
$lenovo->identifyUser();



