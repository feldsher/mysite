<?php
/**
 * Created by PhpStorm.
 * User: feldsher
 * Date: 07.11.17
 * Time: 20:11
 */
$delimiter = "-----------------------------------";
function drawDelimiter($delimiter) {
  global $delimiter;
  echo $delimiter."<br>";
}

$content = 'Content string';

$drawContentString = function () use ($content) {
  echo $content ."<br>";
};

drawDelimiter();
$drawContentString();
drawDelimiter();