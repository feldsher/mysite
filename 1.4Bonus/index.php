<?php
/**
 * Created by PhpStorm.
 * User: feldsher
 * Date: 09.11.17
 * Time: 19:04
 */


function getMonthName($monthNumber = null)
{
  $months = [
    1 => 'January',
    2 => 'February',
    3 => 'March',
    4 => 'April',
    5 => 'May',
    6 => 'June',
    7 => 'July',
    8 => 'August',
    9 => 'September',
    10 => 'October',
    11 => 'November',
    12 => 'December',
  ];

  if ((int)$monthNumber > max(array_keys($months))) {
    return false;
  }

  $monthNumber = (int)($monthNumber ?: date('m'));
  return isset($months[$monthNumber]) ? $months[$monthNumber] : false;
}

function getWeekDays()
{
  return [
    1 => 'Monday',
    2 => 'Tuesday',
    3 => 'Wednesday ',
    4 => 'Thursday',
    5 => 'Friday',
    6 => 'Saturday',
    7 => 'Sunday'
  ];
}

function getMonthCalendar($monthNumber = null, $year = null)
{
  $monthNumber = (int)($monthNumber ?: date('m'));
  $year = (int)($year ?: date('Y'));

  $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $monthNumber, $year);

  $calendar = [];
  $week = [];

  for ($day = 1; $day <= $daysInMonth; $day++) {
    $date = "{$year}-{$monthNumber}-{$day}";
    $dayNumber = (int)date('N', strtotime($date));
    if (empty($week) && $dayNumber > 1) {
      for ($i = 1; $i < $dayNumber; $i++) {
        $week[$i] = null;
      }
    }

    $week[$dayNumber] = $day;

    if ($day === $daysInMonth) {
      while ($dayNumber < 7) {
        $dayNumber++;
        $week[$dayNumber] = null;
      }
    }

    if ($dayNumber === 7) {
      array_push($calendar, $week);
      $week = [];
    }
  }

  return $calendar;
}

function getYearCalendar($year = null, $startMonth = 1, $endMonth = 12)
{
  $calendar = [];

  $month = 1;
  while ($monthName = getMonthName($month)) {
    if ($month < $startMonth) {
      $month++;
      continue;
    } elseif ($month > $endMonth) {
      break;
    }

    $calendar[$monthName] = getMonthCalendar($month, $year);
    $month++;
  }

  return $calendar;
}

function buildCalendarTable($year = null, $startMonth = 1, $endMonth = 12)
{
  $year = (int)($year ?: date('Y'));
  $calendar = '';

  $months = getYearCalendar($year, $startMonth, $endMonth);
  $daysRow = ' <tr>';
  foreach (getWeekDays() as $day) {
    $daysRow .= ' <td>' . $day . ' </td >';
  }
  $daysRow .= '</tr>';

  foreach ($months as $month => $weeks) {
    $calendar .= '<table border="1" cellpadding="5" cellspacing="0">';
    $calendar .= '<tr><td colspan="7" align="center"> ' . $month . ' ' . $year . ' </td></tr>';
    $calendar .= '<tr> ' . $daysRow . '</tr> ';

    foreach ($weeks as $days) {
      $calendar .= '<tr> ';

      foreach ($days as $day) {
        $calendar .= '<td> ' . $day . '</td> ';
      }

      $calendar .= '</tr>';
    }

    $calendar .= '</table>';
  }

  return $calendar;
}

echo buildCalendarTable(2016, 5, 8);