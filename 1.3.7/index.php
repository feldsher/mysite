<?php
/**
 * Created by PhpStorm.
 * User: feldsher
 * Date: 02.11.17
 * Time: 21:16
 */
function Sum(...$a){
  echo implode(' + ', $a) . ' = ' . array_sum($a) . PHP_EOL;
    //echo "<br>".array_sum($a);
}
Sum(1,2,3,4,5,6);
echo "<br>";
echo "######################################################";
function multiply($firstNumber, $secondNumber)
{
  $result = $firstNumber * $secondNumber;
  echo $firstNumber . ' * ' . $secondNumber . ' = ' . $result . PHP_EOL;
}

$firstNumber = mt_rand(0, 100);
$secondNumber = mt_rand(0, 100);
multiply($firstNumber, $secondNumber);